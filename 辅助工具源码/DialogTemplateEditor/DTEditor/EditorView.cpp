// EditorDoc.cpp : implementation of the EditorView class
//

#include "stdafx.h"
#include "DTEditor.h"

#include "EditorDoc.h"
#include "EditorView.h"
#include "Ctrl.h"
#include "CtrlMgt.h"
#include "AttributeDlg.h"
#include "AccellListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// EditorView

IMPLEMENT_DYNCREATE(CEditorView, CView)

BEGIN_MESSAGE_MAP(CEditorView, CView)
	//{{AFX_MSG_MAP(CEditorView)
	ON_COMMAND(ID_ADD_STATIC, OnAddStatic)
	ON_COMMAND(ID_ADD_BUTTON, OnAddButton)
	ON_COMMAND(ID_ADD_EDIT, OnAddEdit)
	ON_COMMAND(ID_ADD_LIST, OnAddList)
	ON_COMMAND(ID_ADD_COMBO, OnAddCombo)
	ON_COMMAND(ID_ADD_PROGRESS, OnAddProgress)
	ON_COMMAND(ID_EDIT_ACCELL, OnEditAccell)
	ON_COMMAND(ID_DELETE, OnDelete)
	ON_COMMAND(ID_SETTAB, OnSettab)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_MOVE_UP, OnMoveUp)
	ON_COMMAND(ID_MOVE_DOWN, OnMoveDown)
	ON_COMMAND(ID_MOVE_LEFT, OnMoveLeft)
	ON_COMMAND(ID_MOVE_RIGHT, OnMoveRight)
	ON_COMMAND(ID_RESIZE_UP, OnResizeUp)
	ON_COMMAND(ID_RESIZE_DOWN, OnResizeDown)
	ON_COMMAND(ID_RESIZE_LEFT, OnResizeLeft)
	ON_COMMAND(ID_RESIZE_RIGHT, OnResizeRight)
	ON_COMMAND(ID_ADD_IMAGE_BUTTON, OnAddImageButton)
	ON_COMMAND(ID_ADD_CHECK_BOX, OnAddCheckBox)
	//}}AFX_MSG_MAP
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditorView construction/destruction
CEditorView::CEditorView()
{
	m_bAssistLine = FALSE;
	m_pAccellListDlg = new CAccellListDlg(this);
	m_pAttributeDlg = new CAttributeDlg(this);
	m_pCtrlMgt = new CCtrlMgt(this);

	// 获取程序运行的当前路径
	char path[256];

	GetCurrentDirectory(256, path);
	if (path[strlen(path) - 1] != '\\') {
		strcat (path, "\\");
	}

	strcpy (m_KeyMapPath, path);
	strcat (m_KeyMapPath, KEYMAP_FILENAME);
}

CEditorView::~CEditorView()
{
	if (m_pAccellListDlg) {
		delete m_pAccellListDlg;
	}
	if (m_pAttributeDlg) {
		delete m_pAttributeDlg;
	}
	if (m_pCtrlMgt) {
		delete m_pCtrlMgt;
	}
}

BOOL CEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// EditorView drawing

void CEditorView::OnDraw(CDC* pDC)
{
	static BOOL bFirst = TRUE;
	if (bFirst)
	{
		bFirst = FALSE;

		m_MemDC.CreateCompatibleDC(pDC);
		CBitmap bmp;
		bmp.CreateCompatibleBitmap(pDC, CLIENT_W, CLIENT_H);
		CBitmap* pOldBmp = m_MemDC.SelectObject(&bmp);
		::DeleteObject(pOldBmp);

		m_hSizeAll = LoadCursor (NULL, IDC_SIZEALL);	// 指向四个方向的箭头
		m_hSizeNS  = LoadCursor (NULL, IDC_SIZENS);		// 指向上下的双向箭头
		m_hSizeWE  = LoadCursor (NULL, IDC_SIZEWE);		// 指向左右的双向箭头
		m_hArrow   = LoadCursor (NULL, IDC_ARROW);		// 原始的鼠标箭头

		m_pAttributeDlg->Create();
		m_pCtrlMgt->New();
	}
	
	// 绘制浅蓝色的边界外围
	m_MemDC.FillSolidRect(0, 0, CLIENT_W, CLIENT_H,RGB(200,200,255));
	m_MemDC.FillSolidRect(SCREEN_LEFT, SCREEN_TOP, SCREEN_W, SCREEN_H, RGB(255,255,255));

	// 在buffer上绘制各个控件
	m_pCtrlMgt->Show(&m_MemDC);

	if (m_bAssistLine)
	{
		// 在屏幕上绘制纵横两条辅助线
		DrawAssistLine(&m_MemDC);
	}

	// 将绘制好的buffer送上屏幕
	pDC->BitBlt(0, 0, CLIENT_W, CLIENT_H, &m_MemDC, 0, 0, SRCCOPY);
}

BOOL CEditorView::DestroyWindow() 
{
	m_pAttributeDlg->DestroyWindow();
	return CView::DestroyWindow();
}

void CEditorView::SetCurCursor ()
{
	switch (m_iCursor)
	{
	case CURSOR_MOVE:	// 移动
		{
			::SetCursor (m_hSizeAll);
			break;
		}
	case CURSOR_U_D:	// 上下
		{
			::SetCursor (m_hSizeNS);
			break;
		}
	case CURSOR_L_R:	// 左右
		{
			::SetCursor (m_hSizeWE);
			break;
		}
	}
}

void CEditorView::Display()
{
	CDC* pdc = GetDC();
	OnDraw (pdc);
	ReleaseDC (pdc);
}

// 设置修改标志
void CEditorView::SetModified()
{
	CEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	pDoc->SetModifiedFlag(TRUE);
	m_bModified = TRUE;
}

// 清除修改标志
void CEditorView::ClrModified()
{
	CEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	pDoc->SetModifiedFlag(FALSE);
	m_bModified = FALSE;
}

// 得到修改标志
BOOL CEditorView::IsModified()
{
	return m_bModified;
}	
/////////////////////////////////////////////////////////////////////////////
// EditorView diagnostics

#ifdef _DEBUG
void CEditorView::AssertValid() const
{
	CView::AssertValid();
}

void CEditorView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CEditorDoc* CEditorView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CEditorDoc)));
	return (CEditorDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// EditorView message handlers

// 添加一个静态文本
void CEditorView::OnAddStatic() 
{
	// 添加一个静态文字框
	m_pCtrlMgt->Add(WND_TYPE_STATIC);
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

// 添加一个按钮
void CEditorView::OnAddButton() 
{
	// 添加一个按钮
	m_pCtrlMgt->Add(WND_TYPE_BUTTON);
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

// 添加一个编辑框
void CEditorView::OnAddEdit() 
{
	// 添加一个编辑框
	m_pCtrlMgt->Add(WND_TYPE_EDIT);
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

// 添加一个列表框
void CEditorView::OnAddList() 
{
	// 添加一个列表框
	m_pCtrlMgt->Add(WND_TYPE_LIST);
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

// 添加一个组合框
void CEditorView::OnAddCombo() 
{
	// 添加一个组合框
	m_pCtrlMgt->Add(WND_TYPE_COMBO);
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

// 添加一个进度条
void CEditorView::OnAddProgress() 
{
	// 添加一个进度条
	m_pCtrlMgt->Add(WND_TYPE_PROGRESS);
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

// 添加一个图像按钮
void CEditorView::OnAddImageButton() 
{
	// 添加一个进度条
	m_pCtrlMgt->Add(WND_TYPE_IMAGE_BUTTON);
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

// 添加一个复选框
void CEditorView::OnAddCheckBox()
{
	// 添加一个进度条
	m_pCtrlMgt->Add(WND_TYPE_CHECK_BOX);
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

// 编辑快捷键列表
void CEditorView::OnEditAccell() 
{
	int nID = -1;

	int nIndex;
	CCtrl* pCtrl;
	if (m_pCtrlMgt->GetCurSel(&pCtrl, &nIndex)) {
		nID = pCtrl->m_ControlInfo.nID;
	}
	
	m_pAccellListDlg->DoModal(nID);
}

// 删除选中的控件
void CEditorView::OnDelete() 
{
	// 删除选中的控件
	if (m_pCtrlMgt->DeleteSelected()) {
		// 显示
		Display();
		// 设置修改标志
		SetModified();
	}
}

// 打开/关闭辅助线
void CEditorView::SwitchAssistLine() 
{
	if (m_bAssistLine == TRUE)
	{
		m_bAssistLine = FALSE;
		Display();
	}
	else
	{
		m_bAssistLine = TRUE;
		CPoint point;
		GetCursorPos (&point);
		ScreenToClient (&point);
		
		if (point.x>0 && point.y>0 && point.x<CLIENT_W && point.y<CLIENT_H)
		{
			m_ptMousePointer = point;

			// 在屏幕上绘制纵横两条辅助线
			Display();
		}
	}
}

// 进入/退出Tab编号状态
void CEditorView::OnSettab() 
{
	// 打开或者关闭Tab序号设置模式
	m_pCtrlMgt->InvertTabMode();
	// 显示
	Display();
}

/* 以下为鼠标操作函数 */

void CEditorView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// 如果落在已经选择的控件上，则不再执行选择操作
	BOOL bPerformSelect = FALSE;
	int iIndex;
	CCtrl* pCtrl;
	if (m_pCtrlMgt->GetCurSel(&pCtrl, &iIndex))
	{
		int iCursor;
		int iMode;
		if (! pCtrl->SetModifySizeCursor(&iCursor, &iMode, point.x, point.y)) {
			// 点在控件范围之外
			bPerformSelect = TRUE;
		}
		else {
			// 点在控件范围之内
			if (pCtrl->IsDialog() && CCtrlMgt::MOVE_CENTER == iMode) {
				//如果当前选择的是对话框，则还可以选对话框上面的控件
				bPerformSelect = TRUE;
			}
		}
	}
	else {
		bPerformSelect = TRUE;
	}

	// 执行选择控件的操作
	if (bPerformSelect)
	{
		m_pCtrlMgt->Select (point.x, point.y);
		// 显示
		Display();

		if (m_pCtrlMgt->m_bIsSetTabMode)
		{
			// 设置修改标志
			SetModified();
		}
	}
	else	// 处理选择拖动功能
	{
		if (! m_pCtrlMgt->m_bIsSetTabMode)
		{
			m_bPick = TRUE;
			// 根据鼠标落点的不同，传递不同的参数
			int iIndex;
			CCtrl* pCtrl;
			if (m_pCtrlMgt->GetCurSel (&pCtrl, &iIndex))
			{
				int iCursor;
				int iMode;
				if (pCtrl->SetModifySizeCursor (&iCursor, &iMode, point.x, point.y))
				{	// 在控件矩形之内
					m_iCursor = iCursor;
					m_pCtrlMgt->SetMouseDragMode (iMode, point.x, point.y);
					SetCurCursor ();	// 设定光标的形状

					// 设定拖拽不能拖拽到屏幕外面去
					CRect ViewRect;
					GetClientRect (&ViewRect);
					ClientToScreen (&ViewRect);
					::ClipCursor (&ViewRect);
				}
				else
				{	// 在控件矩形之外
					m_iCursor = 0;
					SetCurCursor ();
					m_pCtrlMgt->SetMouseDragMode (0, point.x, point.y);
				}
			}
		}
	}

	//
	CView::OnLButtonDown(nFlags, point);
}

void CEditorView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (!m_pCtrlMgt->m_bIsSetTabMode)
	{
		// 清除拖拽有效标志
		m_bPick = FALSE;

		// 恢复鼠标光标
		::SetCursor (m_hArrow);

		// 取消拖拽设定
		m_pCtrlMgt->SetMouseDragMode (0, point.x, point.y);

		// 解除对鼠标移动范围的限定
		::ClipCursor (NULL);

		// 更新属性栏目的显示
		m_pAttributeDlg->Renew(TRUE);
	}
	
	CView::OnLButtonUp(nFlags, point);
}

void CEditorView::OnRButtonUp(UINT nFlags, CPoint point)
{
	SwitchAssistLine();
	CView::OnRButtonUp(nFlags, point);
}

void CEditorView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// 根据拖拽标志确定是否调用控件管理类的拖拽处理函数
	if (m_bPick)
	{
		// 根据光标标志确定显示什么样的光标
		SetCurCursor ();

		m_pCtrlMgt->MouseDrag (point.x, point.y);

		// 显示
		Display();
		// 设置修改标志
		SetModified();
	}
	else
	{
		if (!m_pCtrlMgt->m_bIsSetTabMode)
		{
			// 非Tab编辑状态，根据位置显示不同的光标
			int iIndex;
			CCtrl* pCtrl;
			if (m_pCtrlMgt->GetCurSel (&pCtrl, &iIndex))
			{
				int iCursor;
				int iMode;
				pCtrl->SetModifySizeCursor (&iCursor, &iMode, point.x, point.y);
				m_iCursor = iCursor;
				SetCurCursor ();	// 设定光标的形状
			}
		}
	}
	
	if (m_bAssistLine)
	{
		//刷新辅助线的显示
		m_ptMousePointer = point;
		Display();
	}

	CView::OnMouseMove(nFlags, point);
}

// 移动控件位置或者调整控件的尺寸
void CEditorView::OnMoveUp() 
{
	CCtrl* pCtrl;
	int nIndex;
	if (! m_pCtrlMgt->GetCurSel(&pCtrl, &nIndex))
		return;

	// 调整位置
	pCtrl->m_ControlInfo.nY1 --;
	pCtrl->m_ControlInfo.nY2 --;
	pCtrl->ResizeLevelRects();
	// 修改默认长度
	m_pCtrlMgt->SetDefaultSize(pCtrl);
	// 刷新控件属性列表
	m_pCtrlMgt->RenewAttribute();
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

void CEditorView::OnMoveDown() 
{
	CCtrl* pCtrl;
	int nIndex;
	if (! m_pCtrlMgt->GetCurSel(&pCtrl, &nIndex))
		return;

	// 调整位置
	pCtrl->m_ControlInfo.nY1 ++;
	pCtrl->m_ControlInfo.nY2 ++;
	pCtrl->ResizeLevelRects();
	// 修改默认长度
	m_pCtrlMgt->SetDefaultSize(pCtrl);
	// 刷新控件属性列表
	m_pCtrlMgt->RenewAttribute();
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

void CEditorView::OnMoveLeft() 
{
	CCtrl* pCtrl;
	int nIndex;
	if (! m_pCtrlMgt->GetCurSel(&pCtrl, &nIndex))
		return;

	// 调整位置
	pCtrl->m_ControlInfo.nX1 --;
	pCtrl->m_ControlInfo.nX2 --;
	pCtrl->ResizeLevelRects();
	// 修改默认长度
	m_pCtrlMgt->SetDefaultSize(pCtrl);
	// 刷新控件属性列表
	m_pCtrlMgt->RenewAttribute();
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

void CEditorView::OnMoveRight() 
{
	CCtrl* pCtrl;
	int nIndex;
	if (! m_pCtrlMgt->GetCurSel(&pCtrl, &nIndex))
		return;

	// 调整位置
	pCtrl->m_ControlInfo.nX1 ++;
	pCtrl->m_ControlInfo.nX2 ++;
	pCtrl->ResizeLevelRects();
	// 修改默认长度
	m_pCtrlMgt->SetDefaultSize(pCtrl);
	// 刷新控件属性列表
	m_pCtrlMgt->RenewAttribute();
	// 显示
	Display();
	// 设置修改标志
	SetModified();
}

void CEditorView::OnResizeUp() 
{
	CCtrl* pCtrl;
	int nIndex;
	if (! m_pCtrlMgt->GetCurSel(&pCtrl, &nIndex))
		return;

	// 改变尺寸
	if (pCtrl->m_ControlInfo.height() > 1)
	{
		pCtrl->m_ControlInfo.nY2 --;
		pCtrl->ResizeLevelRects();
		// 修改默认长度
		m_pCtrlMgt->SetDefaultSize(pCtrl);
		// 刷新控件属性列表
		m_pCtrlMgt->RenewAttribute();
		// 显示
		Display();
		// 设置修改标志
		SetModified();
	}
}

void CEditorView::OnResizeDown() 
{
	CCtrl* pCtrl;
	int nIndex;
	if (! m_pCtrlMgt->GetCurSel(&pCtrl, &nIndex))
		return;

	// 改变尺寸
	if (pCtrl->m_ControlInfo.nY2 < SCREEN_H)
	{
		pCtrl->m_ControlInfo.nY2 ++;
		pCtrl->ResizeLevelRects();
		// 修改默认长度
		m_pCtrlMgt->SetDefaultSize(pCtrl);
		// 刷新控件属性列表
		m_pCtrlMgt->RenewAttribute();
		// 显示
		Display();
		// 设置修改标志
		SetModified();
	}
}

void CEditorView::OnResizeLeft() 
{
	CCtrl* pCtrl;
	int nIndex;
	if (! m_pCtrlMgt->GetCurSel(&pCtrl, &nIndex))
		return;

	// 改变尺寸
	if (pCtrl->m_ControlInfo.width() > 1)
	{
		pCtrl->m_ControlInfo.nX2 --;
		pCtrl->ResizeLevelRects();
		// 修改默认长度
		m_pCtrlMgt->SetDefaultSize(pCtrl);
		// 刷新控件属性列表
		m_pCtrlMgt->RenewAttribute();
		// 显示
		Display();
		// 设置修改标志
		SetModified();
	}
}

void CEditorView::OnResizeRight() 
{
	CCtrl* pCtrl;
	int nIndex;
	if (! m_pCtrlMgt->GetCurSel(&pCtrl, &nIndex))
		return;

	// 改变尺寸
	if (pCtrl->m_ControlInfo.nX2 < SCREEN_W)
	{
		pCtrl->m_ControlInfo.nX2 ++;
		pCtrl->ResizeLevelRects();
		// 修改默认长度
		m_pCtrlMgt->SetDefaultSize(pCtrl);
		// 刷新控件属性列表
		m_pCtrlMgt->RenewAttribute();
		// 显示
		Display();
		// 设置修改标志
		SetModified();
	}
}

void CEditorView::DrawAssistLine (CDC* pdc)
{
	COLORREF crPen = RGB (0, 0, 255);

	// 使用蓝色画笔
	HPEN hGreenPen = CreatePen(PS_SOLID, 1, crPen);
	DeleteObject(pdc->SelectObject (hGreenPen));

	// 画线
	pdc->MoveTo (m_ptMousePointer.x, 0);
	pdc->LineTo (m_ptMousePointer.x, CLIENT_H);

	pdc->MoveTo (0, m_ptMousePointer.y);
	pdc->LineTo (CLIENT_W, m_ptMousePointer.y);

	// 写坐标文字
	CString strX, strY;
	strX.Format ("x = %d", m_ptMousePointer.x - SCREEN_LEFT);
	strY.Format ("y = %d", m_ptMousePointer.y - SCREEN_TOP);

	pdc->SetTextColor (crPen);
	pdc->SetBkMode (TRANSPARENT);
	pdc->TextOut (m_ptMousePointer.x + 2, 4, strX);
	pdc->TextOut (4, m_ptMousePointer.y + 2, strY);
}

/* END */