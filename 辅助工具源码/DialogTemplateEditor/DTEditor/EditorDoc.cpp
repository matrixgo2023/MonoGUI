// EditorDoc.cpp : implementation of the EditorDoc class
//

#include "stdafx.h"
#include "DTEditor.h"

#include "CtrlMgt.h"
#include "EditorDoc.h"
#include "EditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// EditorDoc

IMPLEMENT_DYNCREATE(CEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CEditorDoc, CDocument)
	//{{AFX_MSG_MAP(EditorDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CEditorDoc, CDocument)
	//{{AFX_DISPATCH_MAP(EditorDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//      DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IDTEditor to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {F665704A-4D37-4E1A-8F5D-B073796DC3C1}
static const IID IID_IDTEditor =
{ 0xf665704a, 0x4d37, 0x4e1a, { 0x8f, 0x5d, 0xb0, 0x73, 0x79, 0x6d, 0xc3, 0xc1 } };

BEGIN_INTERFACE_MAP(CEditorDoc, CDocument)
	INTERFACE_PART(CEditorDoc, IID_IDTEditor, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EditorDoc construction/destruction

CEditorDoc::CEditorDoc()
{
	// TODO: add one-time construction code here

	EnableAutomation();

	AfxOleLockApp();
}

CEditorDoc::~CEditorDoc()
{
	AfxOleUnlockApp();
}


/////////////////////////////////////////////////////////////////////////////
// EditorDoc serialization

void CEditorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// EditorDoc diagnostics

#ifdef _DEBUG
void CEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// EditorDoc commands
// 新建文件
BOOL bFirst = TRUE;
BOOL CEditorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	if (bFirst)
	{
		bFirst = FALSE;
		return TRUE;
	}

	// 得到View类的指针
	CEditorView* pView = GetView();
	ASSERT_VALID(pView);

	// 新建文件
	pView->m_pCtrlMgt->New();

	return TRUE;
}

// 打开文件
BOOL CEditorDoc::OnOpenDocument( LPCTSTR lpszPathName ) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	// 得到View类的指针
	CEditorView* pView = GetView();
	ASSERT_VALID(pView);

	// 打开文件
	if (! pView->m_pCtrlMgt->Open(lpszPathName))
	{
		// 打开文件失败
		MessageBox (pView->m_hWnd, "模板文件打开失败！", "提示信息", MB_OK | MB_ICONWARNING);
		return FALSE;
	}

	return TRUE;
}

// 保存文件
BOOL CEditorDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	// 得到View类的指针
	CEditorView* pView = GetView();
	ASSERT_VALID(pView);

	// 保存文件
	if (! pView->m_pCtrlMgt->Save(lpszPathName))
	{
		MessageBox (pView->m_hWnd, "文件未能正确保存，请检查文件夹的属性以及磁盘控间！", "提示信息", MB_OK | MB_ICONWARNING);
		return FALSE;
	}

	// 清除修改标志
	pView->ClrModified();
	return TRUE;
}

// 得到View类的指针
CEditorView* CEditorDoc::GetView()
{
	CEditorView* pView = (CEditorView*)m_viewList.GetHead();
	return pView;
}
/* END */