// PytransDlg.h : header file
//

#if !defined(AFX_TESTDLG_H__26F2AEDC_D391_408B_8479_48A0FF4891D0__INCLUDED_)
#define AFX_TESTDLG_H__26F2AEDC_D391_408B_8479_48A0FF4891D0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CPyTransDlg dialog

class CPyTransDlg : public CDialog
{
// Construction
public:
	int NewLinkTable();
	CPyTransDlg(CWnd* pParent = NULL);	// standard constructor

	// 从字符串中取得一行
	BOOL GetLine(char* cString, char* cReturn, int k);
	// 从字符串中取得两个“;”之间的部分
	BOOL GetSlice(char* cString, char* cReturn, int k);


// Dialog Data
	//{{AFX_DATA(CPyTransDlg)
	enum { IDD = IDD_PYTRANS_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPyTransDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CPyTransDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTDLG_H__26F2AEDC_D391_408B_8479_48A0FF4891D0__INCLUDED_)
