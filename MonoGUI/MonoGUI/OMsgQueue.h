////////////////////////////////////////////////////////////////////////////////
// @file OMsgQueue.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__OMSGQUEUE_H__)
#define __OMSGQUEUE_H__

typedef struct _O_MSG
{
	class OWindow*	pWnd;
	ULONGLONG		message;
	ULONGLONG		wParam;
	ULONGLONG		lParam;
} O_MSG;

#define MSG_QUEUE_SIZE    (MESSAGE_MAX+1)//循环队列浪费1个单元

class OMsgQueue
{
private:
	int  m_nFront;
	int  m_nRear;
	O_MSG m_arMsgQ[MSG_QUEUE_SIZE];

public:
	OMsgQueue ();
	virtual ~OMsgQueue ();

	// 从消息队列中获取一条消息，取出的消息将被删除
	// 如果是OM_QUIT消息，则返回0，消息队列空了返回-1，其他情况返回1；
	int GetMsg (O_MSG* pMsg);

	// 向消息队列中添加一条消息；
	// 如果消息队列满（消息数量达到了MESSAGE_MAX 所定义的数目），则返回失败；
	BOOL PostMsg (O_MSG* pMsg);

	// 在消息队列中查找指定类型的消息；
	// 如果发现消息队列中有指定类型的消息，则返回TRUE；
	// 该函数主要用在定时器处理上。CheckTimer函数首先检查消息队列中有没有相同的定时器消息，如果没有，再插入新的定时器消息
	BOOL FindMsg (O_MSG* pMsg);

	// 清除消息队列中所有的消息
	BOOL RemoveAll ();
};

#endif // !defined(__OMSGQUEUE_H__)
