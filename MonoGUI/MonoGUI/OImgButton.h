////////////////////////////////////////////////////////////////////////////////
// @file OImgButton.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined (__OIMAGEBUTTON_H__)
#define __OIMAGEBUTTON_H__


class OImgButton : public OWindow
{
private:
	enum { self_type = WND_TYPE_IMAGE_BUTTON };

public:

	// 定义图像按钮所需要的图片
	// 从上到下分别是：
	// 1>Normal样式的图片
	// 2>Focus样式的图片
	// 3>PushDown样式的图片
	BW_IMAGE* m_pImage;

	OImgButton ();
	virtual ~OImgButton ();

	BOOL Create (OWindow* pParent,
		WORD wStyle,
		WORD wStatus,
		int x,
		int y,
		int w,
		int h,
		int ID
	);
	
	// 虚函数，绘制按钮
	virtual void Paint (LCD* pLCD);

	// 虚函数，消息处理
	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

#if defined (MOUSE_SUPPORT)
	// 坐标设备消息处理
	virtual int PtProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);
#endif // defined(MOUSE_SUPPORT)

	// 调入图片
	BOOL SetImage (BW_IMAGE* pImage);

private:
	// 按键被按下的处理
	void OnPushDown ();
};

#endif // !defined(__OIMGBUTTON_H__)
