////////////////////////////////////////////////////////////////////////////////
// @file MonoGUI.h: interface of the MonoGUI system.
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__MONOGUI_H__)
#define __MONOGUI_H__

#if defined(RUN_ENVIRONMENT_LINUX)
#include <termios.h>
#include <unistd.h>
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
#include <windows.h>
#include <io.h>
#include <fileapi.h>
#endif // defined(RUN_ENVIRONMENT_WIN32)

#include <time.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>

// 定义按键宏与按键值对照表的文件名
#define KEY_MAP_FILE "./dtm/keymap.txt"

// 定义当地时区
#define LOCAL_TIME_ZONE         8

// 定义屏幕尺寸
#define SCREEN_W	480
#define SCREEN_H	320

// 系统栏尺寸
#define SYSTEM_BAR_W 64
#define SYSTEM_BAR_H 16

// 滚动条宽度
#define SCROLL_WIDTH 13

// 窗口标题
#define WINDOW_CAPTION_BUFFER_LEN   (256)

// 列表控件
#define LIST_TEXT_MAX_LENGTH	    (256)     // 一行列表框显示内容的长度
#define LIST_ITEM_H                 (HZK_H+1) // 条目的高度

// 消息队列最大深度
#define MESSAGE_MAX                 (60)

// 定时器最大数量
#define TIMER_MAX                   (6)

#define  LENGTH_OF_ASC   3072       /* the number of bytes of the asc font */
#define  LENGTH_OF_HZK   694752     /* the number of bytes of the hzk font */

#define  ASC_FILE               "zhlib/asc12.bin" /* file name of the asc font */
#define  HZK_FILE               "zhlib/gb12song"  /* file name of the hzk font */

// 定义六个输入法库文件的文件名
#define  PINYIN_LIB_FILE        "zhlib/py.db"
#define  EX_PINYIN_LIB_FILE     "zhlib/ex.db"
#define  LIANXIANG_LIB_FILE     "zhlib/lx.db"
#define  PINYIN_INDEX_FILE      "zhlib/py.idx"
#define  EX_PINYIN_INDEX_FILE   "zhlib/ex.idx"
#define  LIANXIANG_INDEX_FILE   "zhlib/lx.idx"

// 定义六个输入法库文件的长度(字节数)
// 注意：
// 这些宏定义仅在目标机上使用，防止因各种原因数据库被篡改
// 在Windows系统下，调用CreateDataTable函数可以生成索引表
// 在Linux系统和其他目标机上，直接打开现有的索引表文件
#define IME_PY_LIB_LENGTH		17814
#define IME_EX_LIB_LENGTH		58706
#define IME_LX_LIB_LENGTH		44172
#define IME_PY_LINES			407
#define IME_EX_LINES			410
#define IME_LX_LINES			2966

typedef void(* FnEventLoop)();   // for DoModal()

#if defined (RUN_ENVIRONMENT_WIN32)
// 仿真窗口的尺寸：1:普通；2:双倍
#define SCREEN_MODE	1
#endif // defined(RUN_ENVIRONMENT_WIN32)

#if defined (RUN_ENVIRONMENT_LINUX)
// 仿真窗口的尺寸：1:普通；2:双倍
#define SCREEN_MODE	2
#endif // defined(RUN_ENVIRONMENT_LINUX)

#ifndef min
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif // min

#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#endif // max

#define DebugPrintf printf

#include "OCommon.h"           // 公共全局函数及数组

#include "KEY.h"               // 键盘消息映射表
#include "KeyMap.h"            // 按键宏定义与按键值对照表
#include "BWImgMgt.h"          // 图像资源管理类
#include "LCD.h"               // 液晶屏操作函数(GAL层)
#include "OAccell.h"           // 快捷键
#include "OCaret.h"            // 脱字符
#include "OScrollBar.h"        // 滚动条
#include "OMsgQueue.h"         // 消息队列
#include "OTimerQueue.h"       // 定时器队列
#include "OWindow.h"           // 窗口
#include "OStatic.h"           // 静态文本控件
#include "OButton.h"           // 按钮控件
#include "OEdit.h"             // 编辑框
#include "OList.h"             // 列表框
#include "OCombo.h"            // 组合框
#include "OProgressBar.h"      // 进度条
#include "OImgButton.h"        // 图像按钮
#include "OCheckBox.h"         // 复选框
#include "OIME.h"              // 输入法窗口
#include "OIME_DB.h"           // 输入法数据库
#include "ODialog.h"           // 对话框
#include "OMsgBoxDialog.h"     // MsgBox对话框
#include "OApp.h"              // 主程序
#include "OClock.h"            // 钟表界面
#include "OSystemBar.h"        // 系统状态条


// 定义特殊功能按键（注意，这些定义必须放在KEY.h的后面）
#define KEY_CLOCK          KEY_F2 // 显示 / 关闭时间窗口
#define KEY_IME_ONOFF      KEY_F3 // 用于打开 / 关闭输入法
#define KEY_IME_PREV       KEY_F4 // 切换到上一种输入法
#define KEY_IME_NEXT       KEY_F5 // 切换到下一种输入法


#endif // !defined(__MONOGUI_H__)

